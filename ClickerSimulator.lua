local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Clicker Simulator")

local Player = game:GetService("Players").LocalPlayer
local RS = game:GetService("ReplicatedStorage")
local WS = game:GetService("Workspace")
local VU = game:GetService("VirtualUser")

local Gamepass = Player.Data.gamepasses

local Main = ui:new("Main")
local Egg = ui:new("Egg")
local Misc = ui:new("Misc")

local function giveZones()
    local zonesTable = {}
    for i, v in next, WS.Zones:GetChildren() do 
        table.insert(zonesTable, v.Name)
     end
    return zonesTable
end

-- Anti Afk
Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

--functions

local function teleportTo(pos) 
    plr.Character.HumanoidRootPart.CFrame = pos.CFrame + Vector3.new(0, 5, 0) 
end


Main:Toggle("Auto Clicker", true, function()
    getsenv(Player.PlayerGui.mainUI.HUDHandler).activateClick()
end)

Main:Toggle("Auto Collect Wheel Prize", true, function()
    if Player.Data.freeSpinTimeLeft.Value == 0 then 
        RS.Events.Client.spinWheel:InvokeServer() 
    end
end)

Main:Toggle("Auto Collect Gifts", true, function()
    for i, v in pairs(getconnections(Player.PlayerGui.randomGiftUI.randomGiftBackground.Background.confirm.MouseButton1Click)) do 
        v.Function() 
    end
end)

Main:Toggle("Auto Collect Achivements", true, function()
    for i, v in next, Player.currentQuests:GetChildren() do
        if v.questCompleted.Value == true then 
            RS.Events.Client.claimQuest:FireServer(v.Name) 
        end
    end
end)

Main:Toggle("Auto Collect Chests (May Cause Lag!)", true, function()
    for _, v in next, WS.Chests:GetChildren() do
        RS.Events.Client.claimChestReward:InvokeServer(v.Name)
    end
end)

Main:Button("Collect Chests", function()
    for _, v in pairs(WS.Chests:GetChildren()) do
        RS.Events.Client.claimChestReward:InvokeServer(v.Name)
    end
end)

Main:Toggle("Auto Buy Jumps", true, function()
    for _, v in next, WS.Clouds:GetChildren() do
        RS.Events.Client.upgrades.upgradeDoubleJump:FireServer(v.Name, 1)
    end
end)

Main:Toggle("Auto Buy Rebirth Buttons & Pets", true, function()
    for _, v in next, shopTable() do 
        RS.Events.Client.purchaseRebirthShopItem:FireServer(v) 
    end
end)

Main:Button("Unlock All Boosts/Gamepasses", function()
    Player.Boosts.DoubleClicks.isActive.Value = true
    Player.Boosts.DoubleGems.isActive.Value = true
    Player.Boosts.DoubleLuck.isActive.Value = true
    Player.Boosts.DoubleShiny.isActive.Value = true
    Player.Boosts.AutoClick.isActive.Value = true
    Gamepass.Value = Gamepass.Value .. ";autoclicker;"
    Gamepass.Value = Gamepass.Value .. ";autorebirth;"
end)

--Egg
local SelectedEgg;

Egg:Dropdown("Egg", { "Beach", "Lava Dragon", "Treasure", "Space Guardian", "Treeland", "Ice Cream", "Frozen Arctic", "Atlantis", "Tropical", "Bee", "Galaxy", "Steampunk", "Robot", "Chemical", "Basic", "Earth", "Sun God", "Flame Overlord", "Holographic", "Music", "Time", "Hacker", "Wild West", "Tech", "Dinosaur", "Egypt", "Samurai", "Rome", "Ice Age", "Mars", "Jungle", "Viking", "Fantasy", "Fairy", "Extraterrestrial", "Dark Fantasy", "Glitch Event"}, true, function(egg)
    SelectedEgg = egg;
    print(SelectedEgg)
end)

Egg:Toggle("Auto Egg (Single)", true, function()
    if SelectedEgg then
        RS.Events.Client.purchaseEgg2:InvokeServer(WS.Eggs[SelectedEgg], false, false)
    end
end)

Egg:Toggle("Auto Egg (Triple)", true, function()
    if SelectedEgg then
        RS.Events.Client.purchaseEgg2:InvokeServer(WS.Eggs[SelectedEgg], false, false, true)
    end
end)

Egg:Toggle("Auto Egg (6)", true, function()
    if SelectedEgg then
        RS.Events.Client.purchaseEgg6:InvokeServer(WS.Eggs[SelectedEgg], false, false, true)
    end
end)

-- MISC

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)

Misc:Button("Redeem Codes", function()
    local codesTable = {"150KCLICKS", "125KLUCK", "100KLIKES", "75KLIKES", "50KLikes", "30klikes", "20KLIKES", "freeautohatch", "175KLIKELUCK", "225KLIKECODE", "200KLIKECODE", "250KLIKECLICKS", "275K2XSHINY", "300SHINYCHANCE", "300DOUBLELUCK", "325CLICKS2","twitter100k", "tokcodeluck12", "LIKECLICK12", "2xlongluck350"}
    for _, v in pairs(codesTable) do 
        RS.Events.Client.useTwitterCode:InvokeServer(v) 
    end
end)

Misc:InputButton("Set WalkSpeed", "Input WalkSpeed Amount", function(state)
    local TB = game:GetService("CoreGui").UI.MainFrame.Containers.Misc["Set WalkSpeed"]["Set WalkSpeed"]["Input WalkSpeed Amount"]

    Player.Character.Humanoid.WalkSpeed = TB.Text
end)

Misc:InputButton("Set JumpPower", "Input JumpPower Amount", function(state)
    local TB2 = game:GetService("CoreGui").UI.MainFrame.Containers.Misc["Set JumpPower"]["Set JumpPower"]["Input JumpPower Amount"]

    Player.Character.Humanoid.JumpPower = TB2.Text
end)
