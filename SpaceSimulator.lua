local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Space Simulator 2")

local Main = ui:new("Main")
local Kill = ui:new("Mobs")
local Teleports = ui:new("Teleports")
local Misc = ui:new("Misc")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

-- Anti Afk

Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

Main:Toggle("Rebirth", true, function()
    WS.RebirthOptions.RebirthScript.Rebirth:FireServer()
end)

Main:Toggle("Chocolate", true, function()
    for i,v in pairs(WS:GetChildren()) do
        if v.Name == "dome" then
            firetouchinterest(Player.Character.HumanoidRootPart, v.TouchInterest.Parent, 0)
            wait()
            firetouchinterest(Player.Character.HumanoidRootPart, v.TouchInterest.Parent, 1)
        end     
    end 
end)

Main:Toggle("Gems", true, function()
    for i,v in pairs(WS:GetChildren()) do
        if v.Name == "Gem" then
            firetouchinterest(Player.Character.HumanoidRootPart, v.TouchInterest.Parent, 0)
            wait()
            firetouchinterest(Player.Character.HumanoidRootPart, v.TouchInterest.Parent, 1)
        end     
    end 
end)


Main:Toggle("Auto Egg (E)", true, function()
    Player.PlayerGui["E-R-T"].MainFrame.RemoteEvent:FireServer("Single")
end)

Main:Toggle("Auto Egg (R)", true, function()
    Player.PlayerGui["E-R-T"].MainFrame.RemoteEvent:FireServer("Triple")
end)

Main:Toggle("Convert", true, function()
    RS.ConverterGuiEvents.Convert:FireServer()
end)

Main:Toggle("Convert 2", true, function()
    RS.ConverterGuiEvents.Convert2:FireServer()
end)

Main:Button("Codes", function()
    local codesTable = {"ghaztguygames", "freestuff", "spacesim2", "subtoredtrite", "freefireworks", "biggestupdate", "moreacorns", "stpatricks", "freeshamrocks", "freechocolates", "happyeaster", "592022", "brickpet"}
    for _, v in pairs(codesTable) do 
        RS.Code:InvokeServer(v) 
    end
end)

Kill:Toggle("Aether Witch", true, function()
    local args = {
        [1] = workspace.Npcs:FindFirstChild("Aether Witch"),
        [2] = 3000000000
    }
    
    RS.EnemyHitEvent:FireServer(unpack(args))
end)

Kill:Toggle("Aether Knight", true, function()
    local args = {
        [1] = workspace.Npcs:FindFirstChild("Aether Knight"),
        [2] = 3000000000
    }
    
    RS.EnemyHitEvent:FireServer(unpack(args))
end)

Kill:Toggle("Aether Titan [Boss]", true, function()
    local args = {
        [1] = workspace.Npcs:FindFirstChild("Aether Titan [Boss]"),
        [2] = 3000000000
    }
    
    RS.EnemyHitEvent:FireServer(unpack(args))
end)
-----------------------Teleport------------------------------------------------------------------------

Teleports:Button("Spawn", function()
    Player.Character.HumanoidRootPart.CFrame = WS.TeleportParts.Spawn.CFrame
end)

Teleports:Button("Egg1", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg1.Egg.CFrame
end)

Teleports:Button("Egg2", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg2.Egg.CFrame
end)

Teleports:Button("Egg3", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg3.Egg.CFrame
end)

Teleports:Button("Egg4", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg4.Egg.CFrame
end)

Teleports:Button("Egg5", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg5.Egg.CFrame
end)

Teleports:Button("Egg6", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg6.Egg.CFrame
end)

Teleports:Button("Egg7", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg7.Egg.CFrame
end)

Teleports:Button("Egg8", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg8.Egg.CFrame
end)

Teleports:Button("Egg9", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg9.Egg.CFrame
end)

Teleports:Button("Egg10", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Egg10.Egg.CFrame
end)


--------------------MISC-------------------------------------------------------------------------------------------------

Misc:Button("Print NPCs", function()
    print("-------------------This is a Devider----------------------------------------------------------------------")
    for i,v in pairs(WS.Npcs:GetChildren()) do
        if v.Data.MaxHealth.Value > 0 then
            print("Name: "..v.Name.." | Max Health: "..v.Data.MaxHealth.Value.."  | Level: "..v.Data.Levels.Value)
        end
    end
end)

Misc:Button("Print Door Code", function()
    print("-----------------------------------------------------------------------------------------------------------")
    print("Door Code: 592021")
end)

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/8zzP4fXNFz")
end)
