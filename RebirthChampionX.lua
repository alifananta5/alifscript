local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Rebirth Champions X")

local Main = ui:new("Main")
local Egg = ui:new("Egg")
local Potions = ui:new("Potions")
local Misc = ui:new("Discord")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

local DailySpinPath = WS.Scripts.DailySpin.Billboard.BillboardGui.Countdown
local PetMachinePath = Player.PlayerGui.MainUI.PetMachineFrame.Top.Holder.Draw.Top.Dark

local PotionsRemote = RS.Events.Potion

-- Anti Afk
Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

Main:Toggle("Auto Click", true, function()
    RS.Events.Click3:FireServer()
end)

Main:Button("Unlock Gamepasses", function()
    for i,v in pairs(Player.Passes:GetChildren()) do
        if v.Value == false then
            v.Value = true
        end
    end
end)

Main:Button("Redeem Codes", function()
    local codesTable = {"space", "easter2", "50m", "easter", "steampunk", "hell", "cave", "175kthanks", "spooky", "void", "nuclear", "thanksfor50k", "10mthanks", "magic", "freeclicksong", "5klikesthanks", "moon", "already1500likes", "thanks500likes", "wow2500likes", "RELEASE", "100kthanks", "175kthanks"}
    for i, v in pairs(codesTable) do 
        RS.Events.Codes:FireServer(v) 
    end
end)

Main:Button("Unlock Worlds", function()
    local Portals = game:GetService("Workspace").Scripts.Portals

    if Portals.Forest.Unlocked.Value == false then
    Portals:FindFirstChild("Forest").Unlocked.Value = true
    Portals:FindFirstChild("Forest").LabelUI:Destroy()
    end
    
    if Portals.Beach.Unlocked.Value == false then
    Portals:FindFirstChild("Beach").Unlocked.Value = true
    Portals:FindFirstChild("Beach").LabelUI:Destroy()
    end
    
    if Portals.Atlantis.Unlocked.Value == false then
    Portals:FindFirstChild("Atlantis").Unlocked.Value = true
    Portals:FindFirstChild("Atlantis").LabelUI:Destroy()
    end
    
    if Portals.Desert.Unlocked.Value == false then
    Portals:FindFirstChild("Desert").Unlocked.Value = true
    Portals:FindFirstChild("Desert").LabelUI:Destroy()
    end
    
    if Portals.Winter.Unlocked.Value == false then
    Portals:FindFirstChild("Winter").Unlocked.Value = true
    Portals:FindFirstChild("Winter").LabelUI:Destroy()
    end

    if Portals.Volcano.Unlocked.Value == false then
    Portals:FindFirstChild("Volcano").Unlocked.Value = true
    Portals:FindFirstChild("Volcano").LabelUI:Destroy()
    end

    if Portals.Moon.Unlocked.Value == false then
    Portals:FindFirstChild("Moon").Unlocked.Value = true
    Portals:FindFirstChild("Moon").LabelUI:Destroy()
    end

    if Portals.Cyber.Unlocked.Value == false then
    Portals:FindFirstChild("Cyber").Unlocked.Value = true
    Portals:FindFirstChild("Cyber").LabelUI:Destroy()
    end

    if Portals.Magic.Unlocked.Value == false then
    Portals:FindFirstChild("Magic").Unlocked.Value = true
    Portals:FindFirstChild("Magic").LabelUI:Destroy()
    end

    if Portals.Heaven.Unlocked.Value == false then
    Portals:FindFirstChild("Heaven").Unlocked.Value = true
    Portals:FindFirstChild("Heaven").LabelUI:Destroy()
    end

    if Portals.Nuclear.Unlocked.Value == false then
    Portals:FindFirstChild("Nuclear").Unlocked.Value = true
    Portals:FindFirstChild("Nuclear").LabelUI:Destroy()
    end

    if Portals.Void.Unlocked.Value == false then
    Portals:FindFirstChild("Void").Unlocked.Value = true
    Portals:FindFirstChild("Void").LabelUI:Destroy()
    end

    if Portals.Spooky.Unlocked.Value == false then
    Portals:FindFirstChild("Spooky").Unlocked.Value = true
    Portals:FindFirstChild("Spooky").LabelUI:Destroy()
    end

    if Portals.Cave.Unlocked.Value == false then
    Portals:FindFirstChild("Cave").Unlocked.Value = true
    Portals:FindFirstChild("Cave").LabelUI:Destroy()
    end

    if Portals.Steampunk.Unlocked.Value == false then
    Portals:FindFirstChild("Steampunk").Unlocked.Value = true
    Portals:FindFirstChild("Steampunk").LabelUI:Destroy()
    end

    if Portals.Hell.Unlocked.Value == false then
    Portals:FindFirstChild("Hell").Unlocked.Value = true
    Portals:FindFirstChild("Hell").LabelUI:Destroy()
    end

    if Portals["50M"].Unlocked.Value == false then
        Portals:FindFirstChild("50M").Unlocked.Value = true
        Portals:FindFirstChild("50M").LabelUI:Destroy()
    end
    
    if Portals["Space"].Unlocked.Value == false then
        Portals:FindFirstChild("Space").Unlocked.Value = true
        Portals:FindFirstChild("Space").LabelUI:Destroy()
    end
end)

Main:Toggle("Auto Daily Spin", true, function()
    if DailySpinPath.Text == "Ready to Claim!" then
        RS.Functions.Spin:InvokeServer()
    end
end)

Main:Toggle("Auto Machine", true, function()
    if PetMachinePath.Visible == false then
        RS.Functions.Machine:InvokeServer()
    end
end)

Main:Toggle("Auto Chests", true, function()
    for i,v in pairs(WS.Scripts.Chests:GetChildren()) do
        if v.Touch.Timer.Countdown.Text == "Ready to Claim!" then
            for ii, vv in pairs(v.Touch:GetChildren()) do
                if vv:IsA("TouchTransmitter") then
                    firetouchinterest(game.Players.LocalPlayer.Character.HumanoidRootPart, vv.Parent, 0)
                    wait()
                    firetouchinterest(game.Players.LocalPlayer.Character.HumanoidRootPart, vv.Parent, 1)
                end
            end
        end
    end
end)

---egg
local SelectedEgg;

Egg:Dropdown("Egg", {"50M Event", "Basic", "King", "Forest", "Beach", "Winter", "Desert", "Volcano", "Lava", "Mythic", "Magma", "Atlantis", "Hell", "Moon", "Cyber", "Magic", "Heaven", "Nuclear", "Void", "Spooky", "Cave", "Steampunk", "Water", "Space", "Fantasty", "Neon", "Shadow", "Destruction", "Sun", "Saturn", "Hacker", "Black", "Aqua", "Axolotls", "UnderWater", "Pixel", "Sea Cave", "Ancient"}, true, function(egg)
    SelectedEgg = egg;
    print(SelectedEgg)
end)

Egg:Toggle("Auto Egg (Single)", true, function()
    if SelectedEgg then
        local egg = {
            [1] = SelectedEgg,
            [2] = "Single"
        }
        RS.Functions.Unbox:InvokeServer(unpack(egg))
    end
end)

Egg:Toggle("Auto Egg (Triple)", true, function()
    if SelectedEgg then
        local egg2 = {
            [1] = SelectedEgg,
            [2] = "Triple"
        }
        RS.Functions.Unbox:InvokeServer(unpack(egg2))
    end
end)

Egg:Toggle("Auto Egg (Four)", true, function()
    if SelectedEgg then
        local egg3 = {
            [1] = SelectedEgg,
            [2] = "Four"
        }
        RS.Functions.Unbox:InvokeServer(unpack(egg2))
    end
end)

Egg:Toggle("Auto Craft All", true, function()
    local AutoCraft = {[1] = "CraftAll", [2] = {}}

    RS.Functions.Request:InvokeServer(unpack(AutoCraft))
end)

---Potions
local SelectedPotion

Potions:Dropdown("Potion", {"x2Clicks", "x2Gems", "x2Luck", "x2Rebirths", "x2PetXP", "x2HatchSpeed"}, true, function(pot)
    SelectedPotion = pot;
    print(SelectedPotion)
end)

Potions:Toggle("Auto Buy Potion", true, function()
    if SelectedPotion then
        PotionsRemote:FireServer(SelectedPotion)
    end
end)

Potions:Toggle("Auto Buy All Potions", true, function()
    local potionTable = {"x2Clicks", "x2Gems", "x2Luck", "x2Rebirths", "x2PetXP", "x2HatchSpeed"}
    for i, v in pairs(potionTable) do 
        PotionsRemote:FireServer(v) 
    end
    --[[
    PotionsRemote:FireServer("x2Clicks")
    PotionsRemote:FireServer("x2Gems")
    PotionsRemote:FireServer("x2Luck")
    PotionsRemote:FireServer("x2Rebirths")
    PotionsRemote:FireServer("x2PetXP")
    PotionsRemote:FireServer("x2HatchSpeed")
    ]]
end)

---Misc

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)
