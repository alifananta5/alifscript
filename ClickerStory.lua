local library = loadstring(game:HttpGet("https://gitlab.com/Ner0ox/versus/-/raw/main/ui/UI.lua", true))()
local ui = library:CreateWindow("Clicker Story")

local Main = ui:new("Main")
local Egg = ui:new("Egg&pets")

local Misc = ui:new("Discord")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

local Remotes = RS.Remotes

-- Tables
local Codes = {"Gravy", "Russo", "Flamingo", "Razor", "SnugLife", "GOLDENWORLDUPDATE14", "UPDATE24", "UPDATE23", "UPDATE22", "UPDATE21", "UPDATE20", "UPDATE19", "UPDATE18", "UPDATE15", "UPDATE12", "DISCO18", "STAR", "TRADINGUPDATE13", "SANTA", "CHRISTMAS", "8M", "7M", "6M", "5M", "4M", "3M", "2M", "1M"}
local Eggs = {}
local Upgrades = {}

-- Remotes
local ClickEvent = Remotes.Click
local CodeEvent = Remotes.RedeemCode
local UpgradeEvent = Remotes.Shop.BuyUpgrade
local ClassEvent = Remotes.Shop.BuyClass

local EvolveEvent = Remotes.Pets.EvolvePet
local EggEvent = Remotes.OpenEgg

local Boss = Remotes.Projectiles
local BossHit = Boss.ProjectileHit

--tables stuff

for i,v in pairs(WS.Eggs:GetChildren()) do
    if v:IsA("Model") then
        table.insert(Eggs, v.Name)
    end
end

for i,v in pairs(Player.PlayerGui.TokenShop.Frame.Upgrades:GetChildren()) do
    if v:IsA("Frame") then
        table.insert(Upgrades, v.Name)
    end
end

-- Anti Afk
Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

-- Main
Main:Toggle("Auto Click", true, function()
    ClickEvent:FireServer()
end)

Main:Toggle("Fast Kill Boss", true, function()
    BossHit:FireServer("Pet", true)
    for i,v in pairs(game:GetService("Workspace").Projectiles:GetChildren()) do
        if v.Name == "PetProjectile" then
            v:Destroy()
        end
    end
end)

Main:Button("Redeem Codes", function()
    for i,v in pairs(Codes) do
        CodeEvent:InvokeServer(v)
    end
end)

local SelectedUpgrade;

Main:Dropdown("Upgrade", Upgrades, true, function(upg)
    SelectedUpgrade = upg
    print(SelectedUpgrade)
end)


Main:Toggle("Auto Upgrade", true, function()
    if SelectedUpgrade then
        if SelectedUpgrade == "+3 Boss Damage" then
            UpgradeEvent:FireServer(1000)
        elseif SelectedUpgrade == "+1 Pet Rarity" then
            UpgradeEvent:FireServer(1001)
        elseif SelectedUpgrade == "Rebirth Multiplier" then
            UpgradeEvent:FireServer(1002)
        elseif SelectedUpgrade == "Lower Class Price" then
            UpgradeEvent:FireServer(1003)
        elseif SelectedUpgrade == "Increased Pet Level Cap" then
            UpgradeEvent:FireServer(1004)
        elseif SelectedUpgrade == "Egg Price Discount" then
            UpgradeEvent:FireServer(1005)
        elseif SelectedUpgrade == "World Price Discount" then
            UpgradeEvent:FireServer(1006)
        elseif SelectedUpgrade == "More Pets Equipped" then
            UpgradeEvent:FireServer(1007)
        end
    end
end)

Main:Toggle("Auto Upgrade All (Token Shop)", true, function()
    local idksomekindoftablethisis = {1000,1001,1002,1003,1004,1005,1006,1007}

    for i,v in pairs(idksomekindoftablethisis) do
        UpgradeEvent:FireServer(v)
    end
end)

Main:Toggle("Auto Upgrade All (Gems Shop)", true, function()
    for x = 1,12 do
        UpgradeEvent:FireServer(x, true)
    end
end)

-- Eggs

local SelectedEgg;

Egg:Dropdown("Egg", Eggs, true, function(egg)
    SelectedEgg = egg
    print(SelectedEgg)
end)

Egg:Toggle("Auto Egg (Single)", true, function()
    if SelectedEgg then
        EggEvent:InvokeServer(SelectedEgg, 1)
    end
end)

Egg:Toggle("Auto Egg (Triple)", true, function()
    if SelectedEgg then
        EggEvent:InvokeServer(SelectedEgg, 3)
    end
end)

Egg:Toggle("Auto Evolve Pets", true, function()
    EvolveEvent:FireServer("All", 1)
end)

-- Misc
Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)
