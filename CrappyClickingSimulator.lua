local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Crappy Clicking Simulator")

local Main = ui:new("Main")
local Misc = ui:new("Discord")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

-- Anti Afk

Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

-- Main
Main:Toggle("Normal Auto Clicker", true, function()
    WS.Events.AddClick:FireServer()
end)

Main:Toggle("OP Auto Clicker", true, function()
    WS.MegaTapScript.GetClick:FireServer()
end)

Main:Toggle("Auto Obby", true, function()
    Player.Character.HumanoidRootPart.CFrame = CFrame.new(-85985.5391, 927.999146, 87716.3828, -0.666506767, 0, -0.745499074, 0, 1, 0, 0.745499074, 0, -0.666506767)
end)

-- Misc

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)
