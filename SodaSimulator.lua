local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Soda Simulator X Alif Ananta")

local Main = ui:new("Main")
local Egg = ui:new("Egg")
local TP = ui:new("Teleport")
local summer = ui:new("Summer")
local Misc = ui:new("Discord")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

local sellWait = false
local equipWait = false

local RanksTxt = "Ranks"
local Rank = Player.PlayerData.Equipped.Ranks
-- Anti Afk
Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)


local TeleportTable = {}

for i,v in pairs(WS.Portals.TP:GetChildren()) do
    if v:IsA("Part") then
        table.insert(TeleportTable, v.Name)
    end
end

local EggsTable = {}

for i,v in pairs(WS.Eggs:GetChildren()) do
    if v:IsA("Model") then
        table.insert(EggsTable, v.Name)
    end
end


Main:Toggle("Auto Fizz", true, function()
    RS.Assets.Events.Shake:FireServer()
end)

Main:Button("Redeem Codes", function()
    local codesTable = {"5klikestyy", "500kvisitceleb", "2klikeswowty", "1klikesyay", "500likestyty", "UPDATE3", "BETA", "300likes", "100likesyay", "TwitterRelease"}
    for i, v in pairs(codesTable) do 
        RS.Assets.Events.RedeemCode:InvokeServer(v) 
    end
end)

Main:Toggle("Auto Sell Fizz", true, function()
    if sellWait == false then
        Player.Character.HumanoidRootPart.CFrame = CFrame.new(-172.550415, 83.4801025, -3265.24756, 0.978664696, 0, 0.205463827, 0, 1, 0, -0.205463827, 0, 0.978664696)
        sellWait = true
        wait(0)
        sellWait = false
    end
end)

Main:Toggle("Auto Collect Crystal", true, function()
    for i,v in pairs(game:GetService("Workspace").CrystalSpawn.CrystalBox:GetDescendants()) do
        for ii,vv in pairs(v:GetChildren()) do
            if vv.Name == "CrystalBox" then
            Player.Character.HumanoidRootPart.CFrame = vv.Main.CFrame
            end
        end
    end
end)

Main:Toggle("Auto Collect Chests", true, function()
    for i,v in pairs(WS.Chests:GetChildren()) do
        if v.Touch.BillboardGui.Bottom.Text == "Ready!" then
            for ii, vv in pairs(v.Touch:GetChildren()) do
                if vv:IsA("TouchTransmitter") then
                    firetouchinterest(game.Players.LocalPlayer.Character.HumanoidRootPart, vv.Parent, 1)
                    wait()
                    firetouchinterest(game.Players.LocalPlayer.Character.HumanoidRootPart, vv.Parent, 1)
                end
            end
        end
    end
end)
-- SHOP

Main:Toggle("Auto Buy Flavors", true, function()
    RS.Assets.Events.BuyAll:InvokeServer("Flavors")
end)

Main:Toggle("Auto Buy Bottles", true, function()
    RS.Assets.Events.BuyAll:InvokeServer("Bottles")
end)

Main:Toggle("Auto Buy Ranks", true, function()
    if Rank.Value == "Noob" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Scientist")
    elseif Rank.Value == "Soda Scientist" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Master")
    elseif Rank.Value == "Soda Master" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Champion")
    elseif Rank.Value == "Soda Champion" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Hero")
    elseif Rank.Value == "Soda Hero" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Legend")
    elseif Rank.Value == "Soda legned" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Demon")
    elseif Rank.Value == "Soda Demon" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda King")
    elseif Rank.Value == "Soda King" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Fanatic")
    elseif Rank.Value == "Soda Fanatic" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Firegod")
    elseif Rank.Value == "Soda Firegod" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Electrogod")
        elseif Rank.Value == "Soda Electrogod" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Flamin' Noob")
        elseif Rank.Value == "Flamin' Noob" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Slimy Hero")
        elseif Rank.Value == "Slimy Hero" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Evil Scientist")
        elseif Rank.Value == "Evil Scientist" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Atlan")
        elseif Rank.Value == "Soda Atlan" then
        RS.Assets.Events.BuyShop:InvokeServer(RanksTxt, "Soda Mage")
    end
end)

-- Rewards

Main:Toggle("Auto Rewards", true, function()
    for i,v in pairs(Player.PlayerGui.Menus.FreeRewardsMenu.MainBody.MainBody:getChildren()) do
        if v:IsA("ImageButton") then
            if v.Claimed.Value == false then
                RS.Assets.Events.GiftInfo:InvokeServer(v.Name)
            end
        end
    end
end)

-- Eggs

local SelectedEgg;

Egg:Dropdown("Egg", EggsTable, true, function(egg)
    SelectedEgg = egg;
    print(SelectedEgg)
end)

Egg:Toggle("Auto Egg (Single)", true, function()
    if SelectedEgg then
        RS.Assets.Events.OpenEgg:FireServer(SelectedEgg, 1)
    end
end)

Egg:Toggle("Auto Egg (Triple)", true, function()
    if SelectedEgg then
        RS.Assets.Events.OpenEgg:FireServer(SelectedEgg, 3)
    end
end)

Egg:Toggle("Auto Equip Best", true, function()
    if equipWait == false then
        RS.Assets.Events.EquipBestPets:InvokeServer()
        equipWait = true
        wait(5)
        equipWait = false
    end
end)

Egg:Toggle("Auto Craft Pets", true, function()
    RS.Assets.Events.EvolveAllPets:InvokeServer()
end)

-- Teleport

local SelectedTP;

TP:Dropdown("Teleport", TeleportTable, true, function(tele)
    SelectedTP = tele;
    print(SelectedTP)
end)

TP:Button("Teleport", function()
    Player.Character.HumanoidRootPart.CFrame = WS.Portals.TP:FindFirstChild(SelectedTP).CFrame
end)

-- Summer

summer:Toggle("Auto Collect Flowers", true, function()
    for i,v in pairs(game:GetService("Workspace").FlowerSpawn.Flower:GetDescendants()) do
        if v:IsA("Model") then
            for ii,vv in pairs(v:GetChildren()) do
                Player.Character.HumanoidRootPart.CFrame = vv.CFrame
            end
        end
    end
end)

local BuyEvent = RS.Assets.Events.BuyEventItem
local thistable = {"x3 Coins", "Hatch Speed", "Auto Clicker", "x3 Flowers", "Chat Tag", "King Ice Cream Demon"}

local SelectedThing;

summer:Dropdown("SummerShop", thistable, true, function(sele)
    SelectedThing = sele;
    print(SelectedThing)
end)

summer:Toggle("Auto Buy Selected SummerShop", true, function()
    if SelectedThing then
        BuyEvent:InvokeServer(SelectedThing)
    end
end)

summer:Toggle("Auto Buy All SummerShop", true, function()
    for i, v in pairs(thistable) do 
        BuyEvent:InvokeServer(v) 
    end
end)

-- Discord

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)
