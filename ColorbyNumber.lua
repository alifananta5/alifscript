local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Color by Number")

local Main = ui:new("Main")
local Misc = ui:new("Discord")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")

-- Anti Afk
Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

Main:Button("Auto Color All (Button)", function()
    for i,v in pairs(workspace.Map.Blocks["1"].Draw:GetChildren()) do
        game:GetService("ReplicatedStorage").Knit.Services.PixelGeneratorService.RF.DrawPixel:InvokeServer(v)
     end
     for i,v in pairs(workspace.Map.Blocks["2"].Draw:GetChildren()) do
        game:GetService("ReplicatedStorage").Knit.Services.PixelGeneratorService.RF.DrawPixel:InvokeServer(v)
     end
     for i,v in pairs(workspace.Map.Blocks["3"].Draw:GetChildren()) do
      game:GetService("ReplicatedStorage").Knit.Services.PixelGeneratorService.RF.DrawPixel:InvokeServer(v)
   end
   for i,v in pairs(workspace.Map.Blocks["4"].Draw:GetChildren()) do
      game:GetService("ReplicatedStorage").Knit.Services.PixelGeneratorService.RF.DrawPixel:InvokeServer(v)
   end
   for i,v in pairs(workspace.Map.Blocks["5"].Draw:GetChildren()) do
    game:GetService("ReplicatedStorage").Knit.Services.PixelGeneratorService.RF.DrawPixel:InvokeServer(v)
 end
 for i,v in pairs(workspace.Map.Blocks["6"].Draw:GetChildren()) do
    game:GetService("ReplicatedStorage").Knit.Services.PixelGeneratorService.RF.DrawPixel:InvokeServer(v)
 end
end)

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)
