local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Sonic Speed Tycoon")

local main = ui:new("Main")
local Misc = ui:new("Discord")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

-- Anti Afk

Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

main:Toggle("Auto Collect", true, function()
    for i,v in pairs(game:GetService("Workspace").Map.Objects:GetDescendants()) do
        if v.ClassName == "Part" and v.Name == "Hitbox" then
            Player.Character.HumanoidRootPart.CFrame = v.CFrame
            RS.Knit.Services.WorldCurrencyService.RE.PickupCurrency:FireServer(v.Parent.Name)
            v.Parent:Destroy()
        end
    end    
end)

main:Toggle("Auto Steps", true, function()
    local ohTable1 = {
        ["Character"] = Player.Character,
        ["CFrame"] = CFrame.new(-10303.543, 117, -6243.03516, 0.952583671, 2.99707096e-07, 0.304276735, 4.91250546e-07, 1, -2.52291488e-06, -0.304276735, 2.55276359e-06, 0.952583611),
        ["IsRunning"] = true
    }
    RS.Knit.Services.CharacterService.RE.UpdateCharacterState:FireServer(ohTable1)
end)

main:Toggle("Auto Rewards", true, function()
    for i,v in pairs(Player.PlayerGui.Main.Menus.Gratitude.Container.GiftContainer.Frame:getChildren()) do
        if v:IsA("Frame") then
            if v.Redeem.Visible == true then
                RS.Knit.Services.GratitudeService.RF.AcquireReward:InvokeServer(v.Name)
            end
        end
    end
end)

main:Toggle("Auto Rebirth", true, function()
    if Player.PlayerGui.Main.LargeHUD.Progression.Container.Level["Progress Bar"].ExperienceText.Text == "Max Level! Rebirth Now!" then
        RS.Knit.Services.LevelingService.RF.AttemptRebirth:InvokeServer()
    end
end)

main:Button("Redeem Codes", function()
    local codesTable = {"RIDERS"}
    for _, v in pairs(codesTable) do 
        RS.Knit.Services.RedeemService.RF.RedeemCode:InvokeServer(v) 
    end
end)

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)
