local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Toy Clicking Simulator")

local Main = ui:new("Main")
local Egg = ui:new("Egg")
local Crate = ui:new("Crate")
local Upgrade = ui:new("Upgrade")
local Misc = ui:new("Discord")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

-- Anti Afk
Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

-- Main
Main:Toggle("Auto Click", true, function()
    RS.Knit.Services.ClickService.RF.Click:InvokeServer()
end)

Main:Button("Redeem Codes", function()
    local codesTable = {"CLOVERPET", "Update45", "Update44", "Update43", "Update42", "Update41", "Bubbles", "InstantDarkMatter", "7MEvent", "7MCrate", "StPatricksDayEvent"}
    for i, v in pairs(codesTable) do 
        RS.Knit.Services.CodesService.RF.RedeemCode:InvokeServer(v) 
    end
end)



local SelectedRebirth;

Main:Dropdown("Rebirth", {1, 5, 25, 50, 150, 500, 2500, 10000, 25000, 50000, 100000, 250000, 500000, 1000000, 10000000, 50000000, 100000000, 250000000, 750000000, 2000000000, 5000000000, 10000000000, 25000000000, 50000000000, 100000000000, 250000000000, 1000000000000, 5000000000000, 10000000000000, 100000000000000}, true, function(rebirth)
    SelectedRebirth = rebirth;
    print(SelectedRebirth)
end)

Main:Toggle("Auto Rebirth Selected", true, function()
    if SelectedRebirth then
        RS.Knit.Services.RebirthService.RF.AttemptRebirth:InvokeServer(SelectedRebirth)
    end
end)

-- SuperUpgrade
local SelectedSRebirth;

Main:Dropdown("Super Rebirth", {1, 5, 10, 25, 50, 100, 250, 500, 1000, 5000, 25000, 50000, 75000, 100000, 125000, 250000, 500000, 750000, 1000000, 2000000, 5000000, 7500000, 10000000, 12500000, 15000000}, true, function(Srebirth)
    SelectedSRebirth = Srebirth;
    print(SelectedSRebirth)
end)

Main:Toggle("Auto Super Rebirth Selected", true, function()
    if SelectedSRebirth then
        RS.Knit.Services.RebirthService.RF.AttemptSR:InvokeServer(SelectedSRebirth)
        RS.Knit.Services.PetService.RF.EquipBest:InvokeServer()
    end
end)

local SelectedPotion;

Main:Dropdown("Potion", {"2x Toys", "2x Gems", "2x Rebirths", "2x Hatch Speed", "2x Super Rebirths", "2x Luck"}, true, function(potion)
    SelectedPotion = potion;
    print(SelectedPotion)
end)

Main:Toggle("Auto Buy Selected Potion", true, function()
    if SelectedPotion then
        RS.Knit.Services.PotionService.RF.BuyPotion:InvokeServer(SelectedPotion)
    end
end)

Main:Toggle("Auto Buy All Potions", true, function()
    local potionTable2 = {"2x Toys", "2x Gems", "x2 Rebirths", "2x Hatch Speed", "2x Super Rebirths", "2x Luck"}
    for i, v in pairs(potionTable2) do 
        RS.Knit.Services.PotionService.RF.BuyPotion:InvokeServer(v) 
    end
        --[[
        RS.Knit.Services.PotionService.RF.BuyPotion:InvokeServer("2x Toys")
        RS.Knit.Services.PotionService.RF.BuyPotion:InvokeServer("2x Gems")
        RS.Knit.Services.PotionService.RF.BuyPotion:InvokeServer("2x Rebirths")
        RS.Knit.Services.PotionService.RF.BuyPotion:InvokeServer("2x Hatch Speed")
        RS.Knit.Services.PotionService.RF.BuyPotion:InvokeServer("2x Super Rebirths")
        RS.Knit.Services.PotionService.RF.BuyPotion:InvokeServer("2x Luck")
        ]]
end)

--Egg
local SelectedEgg;

Egg:Dropdown("Egg", {"Common Egg", "Uncommon Egg", "Warrior Egg", "Golden Egg", "Clover Egg", "Easy 7M Egg", "Hard 7M Egg", "Toy Egg", "Moon Egg", "Alien Egg", "Russo Egg", "Emoji Egg", "Sun Egg", "Lava Egg", "Desert Egg", "Cyber Egg", "Gummy Egg", "Candy Egg", "Bubble Egg", "Coconut Egg", "Beach Egg", "Atlantis Egg", "Food Egg", "3M Egg", "2M Egg", "5M Egg", "4M Egg", "Lucky Egg", "Secret Egg", "Youtuber Egg", "Pirate Egg", "200K Egg", "Clown Egg", "1M egg", "American Egg", "Patriotic Egg", "Mech Egg", "Robot Egg", "Valentines Egg", "Potion Egg", "10K Egg", "Imposter Egg", "100K Egg", "150K Egg", "Polar Egg", "Superhero Egg"}, true, function(egg)
    SelectedEgg = egg;
    print(SelectedEgg)
end)

Egg:Toggle("Auto Egg (Single)", true, function()
    if SelectedEgg then
        local egg = {
            [1] = SelectedEgg,
            [2] = false
        }
        RS.Knit.Services.EggService.RF.OpenEgg:InvokeServer(unpack(egg))
    end
end)

Egg:Toggle("Auto Egg (Triple)", true, function()
    if SelectedEgg then
        local egg = {
            [1] = SelectedEgg,
            [2] = true
        }
        RS.Knit.Services.EggService.RF.OpenEgg:InvokeServer(unpack(egg))
    end
end)

Egg:Toggle("Auto Craft All", true, function()
    RS.Knit.Services.PetService.RF.CraftAllPets:InvokeServer()
end)

--Egg:Toggle("Auto Equip Best (30s)", true, function()
--    RS.Knit.Services.PetService.RF.UnequipAll:InvokeServer()
--    RS.Knit.Services.PetService.RF.EquipBest:InvokeServer()
--    wait(30)
--end)

-- Crate

local SelectedCrate;

Crate:Dropdown("Crate", {"Basic Crate", "Epic Crate", "Sparkle Crate", "Dominus Crate", "Food Crate", "Lucky Crate", "100k Crate", "Rainbow Crate", "American Crate", "7M Crate", "Pirate Crate", "Superhero Crate"}, true, function(crate)
    SelectedCrate = crate;
    print(SelectedCrate)
end)

Crate:Toggle("Auto Egg (Single)", true, function()
    if SelectedCrate then
        local crate = {
            [1] = SelectedCrate,
            [2] = false
        }
        RS.Knit.Services.CrateService.RF.OpenCrate:InvokeServer(unpack(crate))
    end
end)

Crate:Toggle("Auto Egg (Triple)", true, function()
    if SelectedCrate then
        local crate = {
            [1] = SelectedCrate,
            [2] = true
        }
        RS.Knit.Services.CrateService.RF.OpenCrate:InvokeServer(unpack(crate))
    end
end)

-- Upgrade

local SelectedUpgrade;

Upgrade:Dropdown("Upgrade", {"Speed", "PetSlots", "GemMultiplier", "EggSpeed", "FreeAutoClicker", "AutoRebirth", "PetEquipped", "2xToys", "5xToys", "10xToys", "25xToys", "100xToys", "150xToys", "PetXP"}, true, function(upgrade)
    SelectedUpgrade = upgrade;
    print(SelectedUpgrade)
end)

Upgrade:Button("Upgrade (Button)", function()
    if SelectedUpgrade then
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer(SelectedUpgrade)
    end
end)

Upgrade:Toggle("Upgrade (Toggle)", true, function()
    if SelectedUpgrade then
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer(SelectedUpgrade)
    end
end)

Upgrade:Toggle("Auto Upgrade (Toggle)", true, function()
    local upgradeTable = {"Speed", "PetSlots", "GemMultiplier", "EggSpeed", "FreeAutoClicker", "AutoRebirth", "2xToys", "5xToys", "PetEquipped", "10xToys", "25xToys", "100xToys", "150xToys", "PetXP"}
    for i, v in pairs(upgradeTable) do 
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer(v) 
    end
    --[[
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("Speed")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("PetSlots")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("GemMultiplier")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("EggSpeed")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("FreeAutoClicker")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("AutoRebirth")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("2xToys")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("5xToys")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("PetEquipped")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("10xToys")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("25xToys")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("100xToys")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("150xToys")
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer("PetXP")
    ]]
end)


-- Super Upgrade

local SelectedSUpgrade;

Upgrade:Dropdown("Super Upgrades", {"SuperRebirthButtons", "KeepAllPets", "2xRebirths", "InstantGolden", "InstantShiny", "InstantRainbow", "InstantGoldly", "InstantRainbow", "InstantInferno", "InstantChaos", "InstantVoid", "InstantObsidian", "InstantDarkMatter", "ImmoralChance", "HatEquipSlots", "HatInventorySlots"}, true, function(supgrade)
    SelectedSUpgrade = supgrade;
    print(SelectedSUpgrade)
end)

Upgrade:Button("Super Upgrade (Button)", function()
    if SelectedSUpgrade then
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer(SelectedSUpgrade)
    end
end)

Upgrade:Toggle("Super Upgrade (Toggle)", true, function()
    if SelectedSUpgrade then
        RS.Knit.Services.UpgradeService.RF.BuyUpgrade:InvokeServer(SelectedSUpgrade)
    end
end)

-- Misc

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)
