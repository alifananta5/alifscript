local library = loadstring(game:HttpGet("https://gitlab.com/Ner0ox/versus/-/raw/main/ui/UI.lua", true))()
local ui = library:CreateWindow("Ninja Tycoon")

local Main = ui:new("Main")
local plr = ui:new("Player")
local Misc = ui:new("Discord")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

-- Anti Afk

Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

local SelectedTeam;

Main:Dropdown("Select Team", {"Grasslands", "Mountains", "Rice Fields", "Winter Woods", "Forest", "Wisteria Forest", "Grasslands.", "Mountains.", "Rice Fields.", "Winter Woods.", "Forest.", "Wisteria Forest."}, true, function(team)
    SelectedTeam = team;
    print(SelectedTeam)
end)

Main:Toggle("Auto Build", true, function()
    for i,v in pairs(game:GetService("Workspace")["TycoonSets"].Tycoons:FindFirstChild(SelectedTeam).Buttons:GetDescendants()) do
        if v:IsA("TouchTransmitter") then 
            firetouchinterest(Player.Character.HumanoidRootPart,v.Parent,0)
            firetouchinterest(Player.Character.HumanoidRootPart,v.Parent,1)
        end 
    end
end)

Main:Toggle("Auto Collect", true, function()
    firetouchinterest(game.Players.LocalPlayer.Character.HumanoidRootPart,game:GetService("Workspace")["TycoonSets"].Tycoons[SelectedTeam].Essentials.Giver,0)
	firetouchinterest(game.Players.LocalPlayer.Character.HumanoidRootPart,game:GetService("Workspace")["TycoonSets"].Tycoons[SelectedTeam].Essentials.Giver,1)
end)

Main:Toggle("Steal All Money", true, function()
    for i,v in pairs(game:GetService("Workspace")["TycoonSets"]:GetDescendants()) do 
		if v:IsA("Part") then 
			if v.Name == "Giver" then 
				if v.Parent.Name == "Essentials" then 
					firetouchinterest(Player.Character.HumanoidRootPart,v,0)
					firetouchinterest(Player.Character.HumanoidRootPart,v,1)
				end 
			end 
		end 
	end
end)

--Player

plr:InputButton("Set WalkSpeed", "Input WalkSpeed Amount", function(state)
    local TB = game:GetService("CoreGui").UI.MainFrame.Containers.Player["Set WalkSpeed"]["Set WalkSpeed"]["Input WalkSpeed Amount"]

    Player.Character.Humanoid.WalkSpeed = TB.Text
end)

plr:InputButton("Set JumpPower", "Input JumpPower Amount", function(state)
    local TB2 = game:GetService("CoreGui").UI.MainFrame.Containers.Player["Set JumpPower"]["Set JumpPower"]["Input JumpPower Amount"]

    Player.Character.Humanoid.JumpPower = TB2.Text
end)

plr:InputButton("Teleport to", "Input Player Name", function(state)
    local TB3 = game:GetService("CoreGui").UI.MainFrame.Containers.Player["Teleport to"]["Teleport to"]["Input Player Name"]

    Player.Character.HumanoidRootPart.CFrame = game.Players:FindFirstChild(TB3.Text).Character.HumanoidRootPart.CFrame
end)


--Discord

Misc:Button("Copy Discord Invite", function()
    setclipboard("https://discord.gg/eHmdxXS9BD")
end)
