local library = loadstring(game:HttpGet("https://gitlab.com/alifananta5/alifscript/-/raw/main/GamesUI.lua", true))()
local ui = library:CreateWindow("Millionaire Empire Tycoon")

local Main = ui:new("Main")
local Tycoons  = ui:new("Tycoon")

local VU = game:GetService("VirtualUser")
local Player = game:GetService("Players").LocalPlayer
local WS = game:GetService("Workspace")
local RS = game:GetService("ReplicatedStorage")

-- Anti Afk

Player.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)

Main:InputButton("Give Cash", "Input Cash Amount", function(state)
    local Player = game.Players.LocalPlayer
    local TB = game:GetService("CoreGui").UI.MainFrame.Containers.Main["Give Cash"]["Give Cash"]["Input Cash Amount"]

	game.ReplicatedStorage.RespawnService:FireServer(TB.Text)
end)

Main:Button("Redeem Codes", function()
    local codesTable = {"iLift500"}
    for _, v in pairs(codesTable) do 
        game:GetService("ReplicatedStorage").NewCodeEvent:FireServer(v) 
    end
end)

local SelectedTycoon;

Tycoons:Dropdown("Tycoon", {"Tycoon 1", "Tycoon 2", "Tycoon 3", "Tycoon 4"}, true, function(tycoon)
    SelectedTycoon = tycoon;
    print(SelectedTycoon)
end)

Tycoons:Toggle("Auto Collect", true, function()
    if SelectedTycoon then
        for i,v in pairs(game:GetService("Workspace").Tycoons[SelectedTycoon].StarterParts.Collector.Givers.Giver:getChildren()) do
            if v:IsA("TouchTransmitter") then
                firetouchinterest(game.Players.LocalPlayer.Character.HumanoidRootPart, v.Parent, 0)
                wait()
                firetouchinterest(game.Players.LocalPlayer.Character.HumanoidRootPart, v.Parent, 1)
            end
        end
    end
end)

Tycoons:Toggle("Auto Rebirth", true, function()
    game:GetService("ReplicatedStorage").rebirthEvent:FireServer()
    wait(1)
end)

Tycoons:Toggle("Auto Buy", true, function()
    local Player = game.Players.LocalPlayer
    if SelectedTycoon then
        for i,v in pairs(game:GetService("Workspace").Tycoons[SelectedTycoon].ButtonsFolder:getChildren()) do
            if v.Name == "Insane Upgrader" or v.Name == "Godly Weapon" or v.Name == "Double This Floors Income" or v.Name == "Tip Banker (FAST MONEY)" or v.Name == "Boost All Workers [Fast Cash]" or v.Name == "Upgrade Bitcoin Miner [X5 Cash]" or v.Name == "Golden Server [Boosts All Bitcoin Miners]" or v.Name == "Double Doge Income " or v.Name == "Golden DOGE [TRIPLES ALL DOGE MINERS]" or v.Name == "Millionaire Maker - $1M/SECOND!" or v.Name == "Double Income [Floor1]" then
            else
                Player.Character.HumanoidRootPart.CFrame = v.Head.CFrame
                wait(0.1)
            end
        end
    end
end)

Tycoons:Toggle("Rebirth Fucker", true, function()
    local Player = game.Players.LocalPlayer

    game.ReplicatedStorage.RespawnService:FireServer(9999999999999999)
    --wait(0.2)
    game:GetService("ReplicatedStorage").rebirthEvent:FireServer()
end)
