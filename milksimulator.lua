local Library = loadstring(game:HttpGet("https://raw.githubusercontent.com/xHeptc/Kavo-UI-Library/main/source.lua"))()
local Window = Library.CreateLib("                     Milk Simulator GUI  |  By Alif Ananta", "Ocean")
local Tab = Window:NewTab("Execute First") 
local Section = Tab:NewSection("Click These Buttons First")

Section:NewButton("Anti-AFK", "Never get kicked for being idle", function()
    local vu = game:GetService("VirtualUser")
    game:GetService("Players").LocalPlayer.Idled:connect(function()
    vu:Button2Down(Vector2.new(0,0),workspace.CurrentCamera.CFrame)
    wait(1)
    vu:Button2Up(Vector2.new(0,0),workspace.CurrentCamera.CFrame)
    end)
end)

Section:NewButton("Codes", "Redem Code", function()
wait(0.5)
local A_1 = "COW"
local Event = game:GetService("ReplicatedStorage").Knit.Services.CodeService.RF.Redeem
Event:InvokeServer(A_1)
wait(0.5)
local A_1 = "MILK"
local Event = game:GetService("ReplicatedStorage").Knit.Services.CodeService.RF.Redeem
Event:InvokeServer(A_1)
wait(0.5)
local A_1 = "2MVISITS"
local Event = game:GetService("ReplicatedStorage").Knit.Services.CodeService.RF.Redeem
Event:InvokeServer(A_1)
wait(0.5)
local A_1 = "5KLIKES"
local Event = game:GetService("ReplicatedStorage").Knit.Services.CodeService.RF.Redeem
Event:InvokeServer(A_1)
wait(0.5)
local A_1 = "GAMINGDAN"
local Event = game:GetService("ReplicatedStorage").Knit.Services.CodeService.RF.Redeem
Event:InvokeServer(A_1)
end)

local Tab = Window:NewTab("Auto Farm")

local Section = Tab:NewSection("Auto Milk")

Section:NewToggle("Auto Click", "Auto Milks For You", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        local Event = game:GetService("ReplicatedStorage").Knit.Services.MilkService.RE.Drink
        Event:FireServer()
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        local Event = game:GetService("ReplicatedStorage").Knit.Services.MilkService.RE.Drink
        Event:FireServer()
        wait()
        end
    end
end)

local Section = Tab:NewSection("Auto Sell")

Section:NewToggle("Auto Sell", "Auto Sell Milks For You", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        local Event = game:GetService("ReplicatedStorage").Knit.Services.SellService.RE.Sell
        Event:FireServer()
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        local Event = game:GetService("ReplicatedStorage").Knit.Services.SellService.RE.Sell
        Event:FireServer()
        wait()
        end
    end
end)

local Section = Tab:NewSection("Diamonds")

Section:NewToggle("Tp to Diamonds", "Teleports to all the diamonds", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        for i,v in pairs(game:GetService("Workspace").Pickups:GetChildren()) do 
        if v.Name == "BasePickup" then
        game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame =  v.HumanoidRootPart.CFrame 
        end
        end
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        for i,v in pairs(game:GetService("Workspace").Pickups:GetChildren()) do 
        if v.Name == "BasePickup" then
        game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame =  v.HumanoidRootPart.CFrame 
        end
        end
        wait()
        end
    end
end)

local Section = Tab:NewSection("Chests")

Section:NewToggle("Auto Claim Daily Chest", "Auto Claim Chests For You", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = game:GetService("Workspace").DailyChest
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ChestService.RF.OpenChest
        Event:InvokeServer(A_1)
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = game:GetService("Workspace").DailyChest
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ChestService.RF.OpenChest
        Event:InvokeServer(A_1)
        wait()
        end
    end
end)

Section:NewToggle("Auto Claim Group Chests", "Auto Claim Chests For You", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = game:GetService("Workspace").GroupChest
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ChestService.RF.OpenChest
        Event:InvokeServer(A_1)
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = game:GetService("Workspace").GroupChest
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ChestService.RF.OpenChest
        Event:InvokeServer(A_1)
        wait()
        end
    end
end)

local Tab = Window:NewTab("Auto Buys")
local Section = Tab:NewSection("Auto Buy Milk")

Section:NewToggle("Auto Buy Milk", "Auto Buy Milk For You", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = "Milk"
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ShopService.RF.BuyAll
        Event:InvokeServer(A_1)
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = "Milk"
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ShopService.RF.BuyAll
        Event:InvokeServer(A_1)
        wait()
        end
    end
end)

local Section = Tab:NewSection("Auto Buy DNA")

Section:NewToggle("Auto Buy DNA", "Auto Buy DNA For You", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = "DNA"
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ShopService.RF.BuyAll
        Event:InvokeServer(A_1)
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = "DNA"
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ShopService.RF.BuyAll
        Event:InvokeServer(A_1)
        wait()
        end
    end
end)

local Section = Tab:NewSection("Auto Buy Class")

Section:NewToggle("Auto Buy Class", "Auto Buy Classes For You", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = "Class"
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ShopService.RF.BuyAll
        Event:InvokeServer(A_1)
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        local A_1 = "Class"
        local Event = game:GetService("ReplicatedStorage").Knit.Services.ShopService.RF.BuyAll
        Event:InvokeServer(A_1)
        wait()
        end
    end
end)

local Section = Tab:NewSection("Auto Buy JetPacks")

Section:NewToggle("Auto Buy JetPacks", "Auto Buy JetPacks For You", function(state)
    if state then
        _G.Condition = true -- true turns it on, false turns it off
        while _G.Condition == true do
        local Event = game:GetService("ReplicatedStorage").Knit.Services.JetpackService.RF.Upgrade
        Event:InvokeServer()
        wait()
        end
    else
        _G.Condition = false -- true turns it on, false turns it off
        while _G.Condition == true do
        local Event = game:GetService("ReplicatedStorage").Knit.Services.JetpackService.RF.Upgrade
        Event:InvokeServer()
        wait()
        end
    end
end)

local Tab = Window:NewTab("Telports") 

local Section = Tab:NewSection("Chests")

Section:NewButton("Normal Chest Tp", "Teleports to the Normal Chest", function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-722.681702, 3.00753093, -999.029785)
end)

Section:NewButton("Group Chest Tp", "Teleports to the Group Chest", function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-650.822632, 3.00753069, -1000.31354)
end)

local Section = Tab:NewSection("Island Telports")

Section:NewButton("Sugar Rush", "Teleports to the SUGAR RUSH Island", function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-763.933838, 466.181976, -1148.27466)
end)

Section:NewButton("Godly Sands", "Teleports to the GODLY SANDS Island", function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-720.067261, 1175.01904, -949.77655)
end)

Section:NewButton("Milky Way", "Teleports to the MILKY WAY Island", function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-639.216125, 1920.02319, -1071.7467)
end)

Section:NewButton("Toy Story", "Teleports to the TOY STORY Island", function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-639.911743, 2522.48145, -989.007874)
end)

Section:NewButton("Cyber", "Teleports to the CYBER Island", function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-729.41095, 3122.90527, -1121.8186)
end)

local Tab = Window:NewTab("Misc.")
local Section = Tab:NewSection("Player Modifications")

Section:NewSlider("WalkSpeed", "Modifies the Walk Speed", 500, 0, function(s) -- 500 (MaxValue) | 0 (MinValue)
    game.Players.LocalPlayer.Character.Humanoid.WalkSpeed = s
end)

Section:NewSlider("Jump Power", "Modifies the Jump Power", 500, 0, function(s) -- 500 (MaxValue) | 0 (MinValue)
    game.Players.LocalPlayer.Character.Humanoid.JumpPower = s
end)

Section:NewButton("Ctrl Click Teleport", "I dont know what to put here :p", function()
    local Plr = game:GetService("Players").LocalPlayer
    local Mouse = Plr:GetMouse()
    Mouse.Button1Down:connect(function()
    if not game:GetService("UserInputService"):IsKeyDown(Enum.KeyCode.LeftControl) then return end
    if not Mouse.Target then return end
    Plr.Character:MoveTo(Mouse.Hit.p)
    end)
end)

Section:NewButton("Infinite Jump", "Unlimited Jumps", function()
    local InfiniteJumpEnabled = true
    game:GetService("UserInputService").JumpRequest:connect(function()
       if InfiniteJumpEnabled then
           game:GetService"Players".LocalPlayer.Character:FindFirstChildOfClass'Humanoid':ChangeState("Jumping")
       end
    end)
end)

Section:NewButton("No Clip (Press E to Toggle)", "Walk Through Shit", function()
    noclip = false
    game:GetService('RunService').Stepped:connect(function()
    if noclip then
    game.Players.LocalPlayer.Character.Humanoid:ChangeState(11)
    end
    end)
    plr = game.Players.LocalPlayer
    mouse = plr:GetMouse()
    mouse.KeyDown:connect(function(key)
    if key == "e" then
    noclip = not noclip
    game.Players.LocalPlayer.Character.Humanoid:ChangeState(11)
    end
    end)
end)

local Tab = Window:NewTab("Credits")
local Section = Tab:NewSection("Scripts - Made By Silxnce (me :p)")
local Section = Tab:NewSection("Kavo UI Library - Made by xHeptc")
